﻿import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {map} from 'rxjs/operators';

import {environment} from '@environments/environment';
import {User} from '@app/_models';

@Injectable({providedIn: 'root'})
export class AuthenticationService {

  public currentUser: Observable<User>;

  private currentUserSubject: BehaviorSubject<User>;

  constructor(private http: HttpClient) {
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  login(email: string, password: string): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    };
    return this.http.post<any>(`${environment.apiUrl}/auth/login`, {email: email, password}, httpOptions)
      .pipe(map(res => {
        if (res.auth === true) {
          localStorage.setItem('token', res.token);
        }
        return res;
      }));
  }

  register(newUser: User) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    };

    return this.http.post<any>(`${environment.apiUrl}/auth/register`, newUser, httpOptions)
      .pipe(map(res => {
        if (res.auth === true) {
          localStorage.setItem('token', res.token);
        }
        return res;
      }));
  }

  getCurrentUserInfo(): Observable<User> {
    return this.http.get<any>(`${environment.apiUrl}/auth/me`)
      .pipe(map(res => {
        localStorage.setItem('currentUser', JSON.stringify(res));
        this.currentUserSubject.next(res);
        return res;
      }));
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
    localStorage.removeItem('token');
    this.currentUserSubject.next(null);
  }
}
