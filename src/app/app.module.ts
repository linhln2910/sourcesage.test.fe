﻿import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {ReactiveFormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';

import {AppComponent} from './app.component';
import {appRoutingModule} from './app.routing';

import {ErrorInterceptor, JwtInterceptor} from './_helpers';
import {ProfileComponent} from './profile';
import {LoginComponent} from './login';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {MatInputModule} from "@angular/material/input";
import {SignupComponent} from "@app/signup";
import {MatSelectModule} from "@angular/material/select";
import {DashboardComponent} from "@app/dashboard";
import {IncomingEnqComponent} from "@app/incoming-enquiries";
import {IncomingQuotationsComponent} from "@app/incoming-quotations";
import {ProductsComponent} from "@app/products";
import {MyRequestComponent} from "@app/my-requests";
import {OrderHistoryComponent} from "@app/order-history";
import {SettingsComponent} from "@app/settings";

@NgModule({
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule,
    appRoutingModule,
    MatInputModule,
    MatSelectModule
  ],
  declarations: [
    AppComponent,
    ProfileComponent,
    SignupComponent,
    LoginComponent,
    DashboardComponent,
    IncomingEnqComponent,
    IncomingQuotationsComponent,
    ProductsComponent,
    MyRequestComponent,
    OrderHistoryComponent,
    SettingsComponent,
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true},
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
