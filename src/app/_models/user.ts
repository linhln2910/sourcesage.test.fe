﻿export class User {
  id: number;
  name: string;
  email: string;
  age: string;
  gender: string;
  password: string;
}
