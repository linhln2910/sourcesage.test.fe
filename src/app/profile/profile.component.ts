﻿import {Component} from '@angular/core';

import {User} from '@app/_models';
import {AuthenticationService} from '@app/_services';

@Component({
  templateUrl: 'profile.component.html',
  styleUrls: ['./profile.component.less'],
})
export class ProfileComponent {

  user: User;

  constructor(private authService: AuthenticationService) {
  }

  ngOnInit() {
    this.authService.getCurrentUserInfo().subscribe(user => {
      this.user = user;
    })
  }
}
