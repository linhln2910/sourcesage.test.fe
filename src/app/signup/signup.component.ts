﻿import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {first} from 'rxjs/operators';

import {AuthenticationService} from '@app/_services';
import {MyErrorStateMatcher} from "@app/_helpers/state-matcher";

export class Gender {
  id: number;
  value: string;
}

@Component({
  templateUrl: 'signup.component.html',
  styleUrls: ['./signup.component.less']
})
export class SignupComponent implements OnInit {

  loginForm: FormGroup;
  genders = [{
    id: 0,
    value: 'Male'
  }, {
    id: 1,
    value: 'Female'
  }] as Gender[];
  error = '';
  success = false;

  matcher = new MyErrorStateMatcher();

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService
  ) {
    // redirect to home if already logged in
    if (this.authenticationService.currentUserValue) {
      this.router.navigate(['/']);
    }
  }

  // convenience getter for easy access to form fields
  get loginFormControls() {
    return this.loginForm.controls;
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email,]],
      password: ['', Validators.required],
      gender: [0],
      age: [''],
    });
  }

  registerUser() {
    if (this.loginForm.invalid) {
      return;
    }

    this.authenticationService.register(this.loginForm.value)
      .pipe(first())
      .subscribe(
        data => {
          this.getUserInfo();
        },
        error => {
          this.error = error;
        });
  }

  getUserInfo() {
    this.authenticationService.getCurrentUserInfo()
      .pipe(first())
      .subscribe(data => {
          this.router.navigate(['/profile']);
        },
        error => {
          this.error = error;
        });
  }
}
