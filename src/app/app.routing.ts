﻿import {RouterModule, Routes} from '@angular/router';

import {ProfileComponent} from './profile';
import {LoginComponent} from './login';
import {AuthGuard} from './_helpers';
import {SignupComponent} from "@app/signup";
import {DashboardComponent} from "@app/dashboard";
import {IncomingEnqComponent} from "@app/incoming-enquiries";
import {IncomingQuotationsComponent} from "@app/incoming-quotations";
import {ProductsComponent} from "@app/products";
import {MyRequestComponent} from "@app/my-requests";
import {OrderHistoryComponent} from "@app/order-history";
import {SettingsComponent} from "@app/settings";

const routes: Routes = [
  {path: '', component: ProfileComponent, canActivate: [AuthGuard]},
  {path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard]},
  {path: 'incoming-enq', component: IncomingEnqComponent, canActivate: [AuthGuard]},
  {path: 'incoming-quotations', component: IncomingQuotationsComponent, canActivate: [AuthGuard]},
  {path: 'products', component: ProductsComponent, canActivate: [AuthGuard]},
  {path: 'my-request', component: MyRequestComponent, canActivate: [AuthGuard]},
  {path: 'order-history', component: OrderHistoryComponent, canActivate: [AuthGuard]},
  {path: 'settings', component: SettingsComponent, canActivate: [AuthGuard]},
  {path: 'profile', component: ProfileComponent, canActivate: [AuthGuard]},

  {path: 'login', component: LoginComponent},
  {path: 'signup', component: SignupComponent},

  // otherwise redirect to home
  {path: '**', redirectTo: 'profile'}
];

export const appRoutingModule = RouterModule.forRoot(routes);
